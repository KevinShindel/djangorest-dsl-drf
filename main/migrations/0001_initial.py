# Generated by Django 3.2.4 on 2021-06-25 08:34

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('country_code', models.CharField(max_length=2, verbose_name='country code')),
                ('created', models.DateField(auto_now=True, verbose_name='created')),
            ],
        ),
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('color', models.CharField(max_length=30, verbose_name='color')),
                ('description', models.TextField(verbose_name='description')),
                ('type', models.IntegerField(choices=[(1, 'Sedan'), (2, 'Truck'), (3, 'SUV')], verbose_name='type')),
                ('manufacturer', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.manufacturer', verbose_name='manufacturer')),
            ],
            options={
                'verbose_name': 'Car',
                'verbose_name_plural': 'Cars',
            },
        ),
    ]
