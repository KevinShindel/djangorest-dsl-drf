from django.db import models
from django.utils.translation import ugettext_lazy as _


class Manufacturer(models.Model):
    name = models.CharField(verbose_name=_('name'), max_length=100)
    country_code = models.CharField(verbose_name=_('country code'), max_length=2)
    created = models.DateField(verbose_name=_('created'), auto_now=True)

    def __str__(self):
        return f'{self.name} [{self.country_code}]'


class Car(models.Model):

    class Meta:
        verbose_name = _('Car')
        verbose_name_plural = _('Cars')

    SEDAN = 1
    TRUCK = 2
    SUV = 3

    TYPE_LIST = (
        (SEDAN, 'Sedan'),
        (TRUCK, 'Truck'),
        (SUV, 'SUV'),
    )

    name = models.CharField(verbose_name=_('name'), max_length=100)
    color = models.CharField(verbose_name=_('color'), max_length=30)
    description = models.TextField(verbose_name=_('description'))
    type = models.IntegerField(verbose_name=_('type'),choices=TYPE_LIST)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE, verbose_name=_('manufacturer'))

    def __str__(self):
        return self.name

    def get_auction_title(self):
        return f'{self.name} - {self.color}'


class Advertising(models.Model):
    title = models.CharField(max_length=255, verbose_name=_('title'))
    description = models.TextField(verbose_name=_('description'))
    link = models.URLField(verbose_name=_('link'))
    car = models.ForeignKey(Car, on_delete=models.CASCADE, verbose_name=_('car'), related_name='advertising')
    created = models.DateField(auto_now_add=True)
    modified = models.DateField(auto_now=True)

    def __str__(self):
        return f'{self.car.name}'
