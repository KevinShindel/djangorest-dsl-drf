from django.contrib import admin
from main.models import Car, Advertising, Manufacturer


@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
    pass


@admin.register(Advertising)
class AdvertisingAdmin(admin.ModelAdmin):
    pass


@admin.register(Manufacturer)
class ManufacturerAdmin(admin.ModelAdmin):
    pass
