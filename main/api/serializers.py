from django_elasticsearch_dsl_drf import serializers

from main.documents import CarDocument


class CarDocumentSerializer(serializers.DocumentSerializer):

    class Meta:
        document = CarDocument
        fields = ('id', 'description', 'manufacturer', 'type', 'name', 'color')
