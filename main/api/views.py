from django_elasticsearch_dsl_drf.filter_backends import (FilteringFilterBackend,
                                                          OrderingFilterBackend,
                                                          SearchFilterBackend)
from django_elasticsearch_dsl_drf.viewsets import DocumentViewSet

from main.api.serializers import CarDocumentSerializer
from main.documents import CarDocument


class CarDocumentView(DocumentViewSet):
    document = CarDocument
    serializer_class = CarDocumentSerializer
    lookup_field = 'id'
    filter_backends = [FilteringFilterBackend,
                       OrderingFilterBackend,
                       SearchFilterBackend]
    search_fields = ('description', 'type', 'name')
    # Define filtering fields
    filter_fields = {
        'id': None,
        'name': 'name.raw',
    }
    # Define ordering fields
    ordering_fields = {
        'id': None,
        'name': None,
    }
    # Specify default ordering
    ordering = ('id', 'name',)
