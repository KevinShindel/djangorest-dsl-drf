from django.urls import path

from main.api.views import CarDocumentView

urlpatterns = [
    path('car/', CarDocumentView.as_view({'get': 'list'}), name='car_list'),
]
