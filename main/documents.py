from django_elasticsearch_dsl import (fields, Document)
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import analyzer

from main.models import Car, Manufacturer, Advertising

html_strip = analyzer(
    'html_strip',
    tokenizer="standard",
    filter=["lowercase", "stop", "snowball"],
    char_filter=["html_strip"]
)


@registry.register_document
class CarDocument(Document):
    # id = fields.IntegerField()  # primary key declaration ( seems duplicated with _id field )
    description = fields.TextField(analyzer=html_strip, fields={'raw': fields.KeywordField()})

    manufacturer = fields.ObjectField(properties={'name': fields.TextField(),'country_code': fields.TextField()})
    advertising = fields.NestedField(properties={'description': fields.TextField(analyzer=html_strip),
                                                 'title': fields.TextField(),
                                                 'pk': fields.IntegerField(),
                                                 })

    type = fields.TextField(attr='get_type_display')  # get string value instead integer

    class Index:  # configurations for DB
        name = 'cars'
        settings = {'number_of_shards': 1, 'number_of_replicas': 0}

    class Django:  # configuration for django model
        model = Car
        fields = ['name', 'color']
        related_models = [Manufacturer, Advertising]  # related models

    @staticmethod
    def get_instances_from_related(related_instance):
        """If related_models is set, define how to retrieve the Car instance(s) from the related model.
        The related_models option should be used with caution because it can lead in the index
        to the updating of a lot of items.
        """
        if isinstance(related_instance, Manufacturer):
            return related_instance.car_set.all()
        elif isinstance(related_instance, Advertising):
            return related_instance.car
